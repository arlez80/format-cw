package format.cw;
import haxe.io.Bytes;
import haxe.io.Output;

/**
 * 書き込みユーティリティ
 * @author あるる（きのもと 結衣）
 */
class WriterUtil 
{
	/**
	 * CardWirth式文字列を書き込む
	 * @param	output	書き込み先
	 * @param	s		SJIS文字列
	 */
	static public function writeCWString( output:Output, s:String ):Void
	{
		output.writeInt32( s.length );
		output.writeString( s );
	}

	/**
	 * CardWirth式バイナリデータを書き込む
	 * @param	output	書き込み先
	 * @param	d		データ
	 */
	static public function writeCWBytes( output:Output, d:Bytes ):Void
	{
		output.writeInt32( d.length );
		output.write( d );
	}
}
