package format.cw.wsm;

import haxe.io.Bytes;

/**
 * WSMデータ
 */
typedef WSM = {
	version:Int,

	bitmap:Bytes,
	name:String,
	description:String,
	author:String,

	needCoupons:Array<String>,

	startArea:Int,
	steps:Array<Step>,
	flags:Array<Flag>,

	reserved:Int,	// 予約済み
	minLevel:Int,	// レベルの下限値
	maxLevel:Int,	// レベルの上限値
};

/**
 * CardWirth ステップ
 */
typedef Step = {
	name:String,
	init:Int,
	strings:Array<String>,
};

/**
 * CardWirth フラグ
 */
typedef Flag = {
	name:String,
	init:Bool,
	strings:Array<String>,
};
