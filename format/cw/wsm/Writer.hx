package format.cw.wsm;

import format.cw.wsm.Data;
import haxe.io.Output;

using format.cw.WriterUtil;

/**
 * WSM Writer
 * @author あるる（きのもと 結衣）
 */
class Writer 
{
	static public var defaultVersion:Int = 4;

	private var o:Output;

	/**
	 * コンストラクタ
	 * @param	output
	 */
	public function new( output:Output )
	{
		this.o = output;
		o.bigEndian = false;
	}

	/**
	 * 書き込む
	 * @param	wsm
	 */
	public function write( wsm:WSM ):Void
	{
		this.o.writeCWBytes( wsm.bitmap );
		this.o.writeCWString( wsm.name );
		this.o.writeCWString( wsm.description );
		this.o.writeCWString( wsm.author );

		this.o.writeCWString( wsm.needCoupons.join( "\r\n" ) );
		this.o.writeInt32( wsm.needCoupons.length );

		this.o.writeInt32( wsm.startArea + wsm.version * 10000 );

		this.o.writeInt32( wsm.steps.length );
		for ( step in wsm.steps ) {
			this.o.writeCWString( step.name );
			this.o.writeInt32( step.init );
			for ( i in 0 ... 10 ) {
				var s = step.strings[i];
				this.o.writeCWString( s );
			}
		}

		this.o.writeInt32( wsm.flags.length );
		for ( flag in wsm.flags ) {
			this.o.writeCWString( flag.name );
			this.o.writeInt8( flag.init ? 1 : 0 );
			for ( i in 0 ... 2 ) {
				var s = flag.strings[i];
				this.o.writeCWString( s );
			}
		}

		this.o.writeInt32( wsm.reserved );
		this.o.writeInt32( wsm.minLevel );
		this.o.writeInt32( wsm.maxLevel );
	}	
}
