package format.cw.wsm;

import haxe.io.Input;
import format.cw.wsm.Data;

using format.cw.ReaderUtil;

/**
 * WSM Reader
 * @author あるる（きのもと 結衣）
 */
class Reader 
{
	/// データソース
	private var data:Input;

	/**
	 * コンストラクタ
	 */
	public function new( data:Input )
	{
		this.data = data;
		this.data.bigEndian = false;
	}

	/**
	 * 読み込む
	 * @return
	 */
	public function read( ):WSM
	{
		var bitmap = this.data.readCWBytes( );
		var name = this.data.readCWString( );
		var description = this.data.readCWString( );
		var author = this.data.readCWString( );

		var needCoupons = this.data.readCWString( ).split( "\r\n" );
		this.data.readInt32( );

		var startArea = this.data.readInt32( );
		var version = Math.floor( startArea / 10000 );
		startArea %= 10000;

		var steps = new Array<Step>( );
		for ( i in 0 ... this.data.readInt32( ) ) {
			var stepName = this.data.readCWString( );
			var init = this.data.readInt32( );
			var strings = new Array<String>( );
			for ( k in 0 ... 10 ) strings.push( this.data.readCWString( ) );
			steps.push({
				name: stepName,
				init: init,
				strings: strings,
			});
		}

		var flags = new Array<Flag>( );
		for ( i in 0 ... this.data.readInt32( ) ) {
			var flagName = this.data.readCWString( );
			var init = this.data.readInt8( );
			var strings = new Array<String>( );
			for ( k in 0 ... 2 ) strings.push( this.data.readCWString( ) );
			flags.push({
				name: flagName,
				init: ( init != 0 ),
				strings: strings,
			});
		}

		var reserved = this.data.readInt32( );
		var minLevel = this.data.readInt32( );
		var maxLevel = this.data.readInt32( );

		return {
			version: version,

			bitmap: bitmap,
			name: name,
			description: description,
			author: author,

			needCoupons: needCoupons,
	
			startArea: startArea,
			steps:steps,
			flags:flags,

			reserved: reserved,
			minLevel: minLevel,
			maxLevel: maxLevel,
		};
	}
}
