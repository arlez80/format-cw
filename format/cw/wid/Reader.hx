package format.cw.wid;

import format.cw.wid.Data;
import haxe.io.Bytes;
import haxe.io.Input;

using format.cw.ReaderUtil;

/**
 * WID Reader
 * @author あるる（きのもと 結衣）
 */
class Reader 
{
	/// データソース
	private var data:Input;

	/**
	 * コンストラクタ
	 */
	public function new( data:Input )
	{
		this.data = data;
		this.data.bigEndian = false;
	}

	/**
	 * 読み込む
	 * @return
	 */
	public function read( ):WID
	{
		var type = this.data.readByte( );
		var bitmap = this.data.readCWBytes( );
		var name = this.data.readCWString( );
		var id = this.data.readInt32( ) % 10000;

		var data:WIDData = switch( type ) {
			case 0: this.readArea( );
			default: null;
		};

		return {
			bitmap: bitmap,
			name: name,
			id: id,

			data: data,
		};
	}

	private function readEventsWithFireCondition( ):EventsWithFireCondition
	{
		var events = this.readEvents( );
		var systemFireEventIDcount = this.data.readInt32( );
		var systemFireEventIDs = new Array<Int>( );
		for( i in 0 ... systemFireEventIDcount ) {
			systemFireEventIDs.push( this.data.readInt32( ) );
		}
		var keyCodes = ReaderUtil.readCWString( this.data );

		return {
			events: events,
			systemFireEventIDs: systemFireEventIDs,
			keycodeEvent: keyCodes.split( "\r\n" ),
		};
	}

	/**
	 * イベントを読み込み
	 * @return
	 */
	private function readEvents( ):Array<Event>
	{
		var eventCount = this.data.readInt32( );
		var events = new Array<Event>( );
		for ( i in 0 ... eventCount ) {
			var event = {
				eventId: 0,
				contentString: "",
				children: [],
				content: Content.start
			};
			event.eventId = this.data.readByte( );
			event.contentString = this.data.readCWString( );
			event.children = this.readEvents( );
			event.content = this.readContent( );
			events.push( event );
		}

		return events;
	}

	/**
	 * コンテント読み込み
	 * @return
	 */
	private function readContent( ):Content
	{
		return switch( this.data.readByte( ) ) {
			case 0x00: Content.start;
			case 0x01: Content.startLink( this.data.readCWString( ) );
			case 0x02: Content.battle( this.data.readInt32( ) );
			case 0x03: Content.end( this.data.readByte( ) == 1 );
			case 0x04: Content.gameover;
			case 0x05: Content.areaMoveTo( this.data.readInt32( ) );
			case 0x06:
				var image = this.data.readCWString( );
				var text = this.data.readCWString( );
				Content.message( image, text );
			case 0x07: Content.bgm( this.data.readCWString( ) );
			case 0x08: Content.background( this.readCells( ) );
			case 0x09: Content.soundEffect( this.data.readCWString( ) );
			case 0x0A: Content.wait( this.data.readInt32( ) );
			case 0x0B:
				var level = this.data.readInt32( );
				var effect = this.data.readByte( );
				var resistance = this.data.readByte( );
				var target = this.data.readByte( );
				var success_rate = this.data.readInt32( );
				var sound = ReaderUtil.readCWString( this.data );
				var visual = this.data.readByte( );
				Content.effect( level, cast effect, cast resistance, cast target, success_rate, sound, cast visual, this.readContentEffectData( ) );
			case 0x0C:
				var member = this.data.readInt32( );
				var select = this.data.readInt32( );
				Content.selectMember( cast member, cast select )
			case 0x0D:

		};
	}

	/**
	 * コンテント効果データを読み込む
	 * @return
	 */
	private function readContentEffectData( ):Array<EffectData>
	{
		
	}

	/**
	 * セルを読み込み
	 * @return
	 */
	private function readCells( ):Array<Cell>
	{
		var cellCount = this.data.readInt32( );
		var cells = new Array<Cell>( );
		for ( i in 0 ... cellCount ) {
			var cell = {
				left:0,
				top:0,
				width:0,
				height:0,
				file:"",
				mask:false,
				flagName:"",
			};

			cell.left = this.data.readInt32( );
			cell.top = this.data.readInt32( );
			cell.width = this.data.readInt32( ) % 10000;
			cell.height = this.data.readInt32( );
			cell.file = this.data.readCWString( );
			cell.mask = this.data.readByte( ) == 1;
			cell.flagName = this.data.readCWString( );

			// 常に0x00
			this.data.readByte( );

			cells.push( cell );
		}

		return cells;
	}

	/**
	 * エリア読み込み
	 * @return
	 */
	private function readArea( ):AreaWID
	{
		var areawid = new AreaWID( );
		areawid.events = this.readEventsWithFireCondition( );
		areawid.positionAdjust = cast this.data.readByte( );
		areawid.menuCards = this.readMenuCards( );
		areawid.cells = this.readCells( );

		return areawid;
	}
}
