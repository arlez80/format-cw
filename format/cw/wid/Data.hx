package format.cw.wid;

import haxe.io.Bytes;

/**
 * 基本 WIDデータ
 */
typedef WID = {
	bitmap:Bytes,
	name:String,
	id:Int,

	data:WIDData,
}

/**
 * WIDデータ
 */
class WIDData {
	public function new( ) {}
}

/**
 * エリア Wid
 */
class AreaWID extends WIDData
{
	public var events:EventsWithFireCondition;
	public var positionAdjust:PositionAdjust;
	public var menuCards:Array<MenuCard>;
	public var cells:Array<Cell>;

	public function new( ) { super( ); }
}

/**
 * バトル Wid
 */
class BattleWID extends WIDData
{
	public var events:EventsWithFireCondition;
	public var positionAdjust:PositionAdjust;
	public var enemyCards:Array<EnemyCard>;
	public var bgm:String;

	public function new( ) { super( ); }
}

/**
 * キャスト Wid
 */
class MateWID extends WIDData
{
	public var level:Int;
	public var reserved:Int;
	public var description:String;
	public var hp:Int;
	public var maxHP:Int;
	public var paralysis:Int;
	public var poison:Int;
	public var fixEvasion:Int;
	public var fixRegistance:Int;
	public var fixDefence:Int;

	public var dexterity:Int;		/// DEX
	public var agility:Int;			/// AGI
	public var intelligence:Int;	/// INT
	public var strength:Int;		/// STR
	public var vitality:Int;		/// VIT
	public var psychology:Int;		/// PSY

	public var bellicose:Int;		/// 好戦さ
	public var cheerful:Int;		/// 陽気さ
	public var courage:Int;			/// 勇敢さ
	public var careful:Int;			/// 慎重さ
	public var sly:Int;				/// 狡猾さ

	public var items:Array<ItemWID>;
	public var skills:Array<SkillWID>;
	public var beasts:Array<BeastWID>;
	public var coupons:Array<Coupon>;

	public function new( ) { super( ); }
}

/**
 * 所持可能用 Wid
 */
class EquipableWID extends WIDData
{
	public var description:String;
	public var physicalFactor:PhysicalFactor;
	public var mentalFactor:MentalFactor;
	public var voice:Bool;
	public var affectRange:AffectRange;
	public var target:Target;

	public var fixSuccess:Int;
	public var visualEffect:Int;
	public var contents:Array<Content>;

	public var fixEvasion:Int;
	public var fixRegistance:Int;
	public var fixDefence:Int;

	public var firstSoundEffect:String;
	public var secondSoundEffect:String;
	public var keycodes:Array<String>;

	public var rare:Int;
	public var scenarioName:String;
	public var author:String;

	public var events:Array<Event>;

	public function new( ) { super( ); }
}

/**
 * アイテム Wid
 */
class ItemWID extends EquipableWID
{
	public var remainUseCount:Int;
	public var maxUseCount:Int;
	public var equipingFixEvasion:Int;
	public var equipingFixRegistance:Int;
	public var equipingFixDefence:Int;

	public function new( ) { super( ); }
}

/**
 * スキル Wid
 */
class SkillWID extends EquipableWID
{
	public var level:Int;
	public var useCount:Int;

	public function new( ) { super( ); }
}

/**
 * 召喚獣 Wid
 */
class BeastWID extends EquipableWID
{
	public var useCount:Int;

	public function new( ) { super( ); }
}

/**
 * 位置調整
 */
@:enum
abstract PositionAdjust(Int) {
	/// 実行時に自動調整
	var auto = 0;
	/// 任意の位置で表示
	var custom = 1;
}

/**
 * 身体的要素
 */
@:enum
abstract PhysicalFactor(Int) {
	var dexterity = 0x01;
	var agility = 0x02;
	var intelligence = 0x04;
	var strength = 0x08;
	var vitality = 0x10;
	var psychology = 0x20;
}

/**
 * 精神的要素
 */
@:enum
abstract MentalFactor(Int) {
	var bellicose = 0x01;		/// 好戦性
	var peaceful = 0x02;		/// 平和性

	var cheerful = 0x04;		/// 社交性
	var shyness = 0x08;			/// 内向性

	var courage = 0x10;			/// 勇猛性
	var cowardice = 0x20;		/// 臆病性

	var careful = 0x40;			/// 慎重性
	var boldness = 0x80;		/// 大胆性

	var sly = 0x100;			/// 狡猾性
	var honestry = 0x200;		/// 正直性
}

/**
 * 範囲効果
 */
@:enum
abstract AffectRange(Int) {
	var single = 0;
	var multiple = 1;
}

/**
 * 対象目標
 */
@:enum
abstract Target(Int) {
	var void = 0;
	var myself = 1;
	var friend = 2;
	var enemy = 3;
	var all = 4;
}

/**
 * 発火イベント集
 */
typedef EventsWithFireCondition = {
	events:Array<Event>,
	systemFireEventIDs:Array<Int>,
	keycodeEvent:Array<String>,
};

/**
 * 称号
 */
typedef Coupon = {
	name:String,
	point:Int,
};

/**
 * イベントツリー
 */
typedef Event = {
	eventId:Int,
	contentString:String,
	children:Array<Event>,
	content:Content
};

/**
 * メニューカードデータ
 */
typedef MenuCard = {
	bitmap:Bytes,
	name:String,
	id:Int,
	description:String,
	events:EventsWithFireCondition,
	flag:String,
	scale:Int,
	left:Int,
	top:Int,
	file:String,
};

/**
 * エネミーカードデータ
 */
typedef EnemyCard = {
	id:Int,
	events:EventsWithFireCondition,
	flag:String,
	scale:Int,
	left:Int,
	top:Int,
	file:String,
};

/**
 * セルデータ
 */
typedef Cell = {
	left:Int,
	top:Int,
	width:Int,
	height:Int,
	file:String,
	mask:Bool,
	flagName:String,
};

/**
 * コンテント
 */
enum Content {
	// Terminal
	start;
	battle( wid:Int );
	end( done:Bool );
	gameover;
	areaMoveTo( nextWid:Int );
	interruption;
	startLink( linkTo:String );
	packageLink( packageWid:Int );

	// Standard
	message( img:String, text:String );
	line( talker:Int, lines:Array<Line> );
	bgm( file:String );
	background( cells:Array<Cell> );
	soundEffect( file:String );
	wait( time:Int );
	effect( level:Int, effect:EffectElement, registance:ResistanceElement, target:SelectTarget, success_rate:Int, sound:String, visual:VisualEffect, effects:Array<EffectData> );
	turnPass;
	startCall( call:String );
	packageCall( packageWid:Int );

	// Data
	branchByFlag( flag:String );
	changeFlag( flag:String, to:Bool );
	reverseFlag( flag:String );
	branchByStep( step:String );
	branchUpAndDownByStep( step:String, value:Int );
	changeStep( step:String, value:Int );
	incStep( step:String );
	decStep( step:String );
	checkFlag( flag:String );

	// Utility
	selectMember( target:SelectMemberTarget, method:SelectMethod );
	checkAbility;	// 未調査
	branchRandom( percent:Int );
	branchByLevel( target:SelectLevelTarget, level:Int );
	branchStatus( target:SelectTarget, status:CheckStatus );
	branchNumber( number:Int );
	branchByArea;
	branchByBattle;
	checkBattle;

	// Branch
	branchByMate( mateWid:Int );
	branchByItem( count:Int, adaptTarget:AdaptTarget, itemWid:Int );
	branchBySkill( count:Int, adaptTarget:AdaptTarget, skillWid:Int );
	branchByInfo( infoWid:Int );
	branchByBeast( count:Int, adaptTarget:AdaptTarget, beastWid:Int );
	branchByMoney( money:Int );
	branchByCoupon( coupon:String, adaptTarget:AdaptTarget );
	branchFinished( scenario:String );
	branchByGossip( gossip:String );

	// Get
	joinMate( mateWid:Int );
	getItem( count:Int, adaptTarget:AdaptTarget, itemWid:Int );
	getSkill( count:Int, adaptTarget:AdaptTarget, skillWid:Int );
	getInfo( infoWid:Int );
	getBeast( count:Int, adaptTarget:AdaptTarget, beastWid:Int );
	getMoney( money:Int );
	getFinished( scenario:String );
	getGossip( gossip:String );

	// Lost
	leaveMate( mateWid:Int );
	lostItem( count:Int, adaptTarget:AdaptTarget, itemWid:Int );
	lostSkill( count:Int, adaptTarget:AdaptTarget, skillWid:Int );
	lostInfo( infoWid:Int );
	lostBeast( count:Int, adaptTarget:AdaptTarget, beastWid:Int );
	lostMoney( money:Int );
	lostFinished( scenario:String );
	lostGossip( gossip:String );

	// Visual
	showParty;
	hideParty;
	redraw;
}

/**
 * セリフデータ
 */
typedef Line = {
	coupon:String,
	text:String,
};

/**
 * 効果コンテント：効果属性
 */
@:enum
abstract EffectElement(Int) {
	var physical = 0;		/// 物理属性
	var magical = 1;		/// 魔法属性
	var magicalPhysics = 2;	/// 魔法的物理属性
	var physicalMagic = 3;	/// 物理的魔法属性
	var void = 4;			/// 無属性
}

/**
 * 効果コンテント：抵抗属性
 */
@:enum
abstract ResistanceElement(Int) {
	var avoid = 0;		/// 回避属性
	var must_hit = 1;	/// 必中属性
	var resistance = 2;	/// 抵抗属性
}

/**
 * 効果コンテント
 */
typedef EffectData = {
	type:Int,
	element:EffectDataElement,
	subNumber:Int,
	subData:EffectSubData,
}

/**
 * 効果コンテントサブデータ
 */
enum EffectSubData {
	none;
	value( type:Int, value:Int );
	continuation( round:Int );
	continuationWithValue( value:Int, round:Int );
	beast( beasts:BeastWID );
}

/**
 * 効果データコンテント: 属性
 */
@:enum
abstract EffectDataElement(Int) {
	var all = 0;
	var physical = 1;
	var mental = 2;
	var holiness = 3;
	var magicPower = 4;
	var fire = 5;
	var ice = 6;
}

/**
 * メンバ選択: 生きているか全員か
 */
@:enum
abstract SelectMemberTarget(Int) {
	var aliveOnly = 0;
	var all = 1;
}

/**
 * メンバレベル選択
 */
@:enum
abstract SelectLevelTarget(Int) {
	var selected = 0;
	var average = 1;
}

/**
 * メンバ選択
 */
@:enum
abstract SelectTarget(Int) {
	var selecting = 0;
	var someone = 1;
	var all = 2;
}

/**
 * 選択方法
 */
@:enum
abstract SelectMethod(Int) {
	var byUser = 0;
	var random = 1;
}

/**
 * 判定状態
 */
@:enum
abstract CheckStatus(Int) {
	var movable = 0x01;
	var unmovable = 0x02;

	var alive = 0x04;
	var dead = 0x08;

	var healthy = 0x10;
	var injury = 0x20;
	var heavyInjury = 0x40;
	var unconscious = 0x80;

	var poison = 0x100;
	var sleep = 0x200;
	var spell = 0x400;
	var paralyse = 0x800;
}

/**
 * 適応対象
 */
@:enum
abstract AdaptTarget(Int) {
	var selecting = 0x01;
	var someone = 0x02;
	var all = 0x04;
	var bag = 0x08;
	var allWithBag = 0x10;
	var field = 0x20;
}

@:enum
abstract VisualEffect(Int) {
	var none = 0;		/// なし
	var reverse = 1;	/// 反転
	var shake = 2;		/// 振動
	var quake = 3;		/// 地震
}
