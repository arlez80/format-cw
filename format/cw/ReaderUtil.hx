package format.cw;

import haxe.io.Bytes;
import haxe.io.Input;

/**
 * 読み込みユーティリティ
 * @author あるる（きのもと 結衣）
 */
class ReaderUtil 
{
	/**
	 * CardWirth式文字列を読み込む
	 * @param	input
	 * @return
	 */
	static public function readCWString( input:Input ):String
	{
		var length = input.readInt32( );
		return input.readString( length );
	}

	/**
	 * CardWirth式バイナリデータを読み込む
	 * @param	input
	 * @return
	 */
	static public function readCWBytes( input:Input ):Bytes
	{
		var length = input.readInt32( );
		return input.read( length );
	}
}
